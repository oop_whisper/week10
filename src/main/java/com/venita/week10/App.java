package com.venita.week10;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        Rectangle rec = new Rectangle(5,3);
        System.out.println(rec.toString());
        System.out.println(rec.calArea());
        System.out.println(rec.calPerimeter());

        Rectangle rec2 = new Rectangle(2,2);
        System.out.println(rec2.toString());
        System.out.println(rec2.calArea());
        System.out.println(rec2.calPerimeter());

        Circle circle1 = new Circle(2);
        System.out.println(circle1);
        System.out.printf("%s area %.3f\n",circle1.getName() ,circle1.calArea());
        System.out.printf("%s perimeter %.3f\n",circle1.getName(),circle1.calPerimeter());

        Circle circle2 = new Circle(3);
        System.out.println(circle2);
        System.out.println(circle2.calArea());
        System.out.println(circle2.calPerimeter());

        Triangle Triangle1 = new Triangle(5,5,6);
        System.out.println(Triangle1.toString());
        System.out.println( Triangle1.getAreaTri());
        System.out.println( Triangle1.getCircumTri());
    }

}
